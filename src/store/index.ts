import { applyMiddleware, compose, createStore } from "redux";
import { Actions, RootState } from "./types";
import middlewares from "./middlewares";
import rootReducer from "./reducers";
import { createWrapper } from "next-redux-wrapper";

declare global {
  interface Window {
    INITIAL_STATE: RootState;
    __REDUX_DEVTOOLS_EXTENSION__?: any;
  }
}

const enhancers =
  typeof window === "object" && window.__REDUX_DEVTOOLS_EXTENSION__
    ? compose(
        applyMiddleware(...middlewares),
        window.__REDUX_DEVTOOLS_EXTENSION__()
      )
    : compose(applyMiddleware(...middlewares));

const makeStore = () => {
  const store = createStore<RootState, Actions, {}, {}>(rootReducer, enhancers);
  return store;
};

export const wrapper = createWrapper(makeStore);
