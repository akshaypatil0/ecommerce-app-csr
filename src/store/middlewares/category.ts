import { Middleware, MiddlewareAPI } from "redux";
import { ThunkDispatch } from "redux-thunk";
import { Actions, RootState } from "../types";
import { ApiCallRequestAction } from "../reducers/api/types";
import { fetchProductsByCategory } from "../reducers/categories";

const category: Middleware<{}, RootState> =
  (store: MiddlewareAPI<ThunkDispatch<RootState, {}, ApiCallRequestAction>>) =>
  (next) =>
  async (action: Actions) => {
    next(action);
    if (action.type === "fetch-all-categories") {
      const { limit, force } = action.meta;
      return Promise.all(
        action.payload.map((category) => {
          return store.dispatch(
            fetchProductsByCategory({ category, limit, force })
          );
        })
      );
    }
  };

export default category;
