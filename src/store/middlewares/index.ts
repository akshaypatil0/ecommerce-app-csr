import thunk from "redux-thunk";
import api from "./api";
import category from "./category";

const middlewares = [thunk, api, category];
export default middlewares;
