import { ActionCreator, Reducer } from "redux";
import { ThunkAction } from "redux-thunk";
import * as url from "../../../utils/urls";
import { ApiCallRequestAction } from "../api/types";
import { RootState } from "../../types";
import { ProductsActions, ProductsState } from "./types";

// Action creators

export const fetchProducts: ActionCreator<
  ThunkAction<
    Promise<void>,
    RootState,
    {},
    ApiCallRequestAction<ProductsActions>
  >
> = ({ limit, force }: { limit?: number; force?: boolean }) => {
  return async (dispatch, getState) => {
    const { lastFetched } = getState().products;

    await dispatch({
      type: "api-call-request",
      payload: {
        url: url.getProducts(limit),
        onRequest: "fetch-products-request",
        onSuccess: "fetch-products-success",
        onFailure: "fetch-products-failed",
      },
      meta: force
        ? {}
        : {
            lastFetched,
            // duration to use cache data in minutes
            cacheDuration: 10,
          },
    });
  };
};

//reducer

const initialState: ProductsState = {
  list: [],
  isFetching: false,
};

const productsReducer: Reducer<ProductsState, ProductsActions> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case "fetch-products-request":
      return {
        ...state,
        isFetching: true,
      };
    case "fetch-products-success":
      return {
        ...state,
        list: action.payload,
        isFetching: false,
        lastFetched: Date.now(),
      };
    case "fetch-products-failed":
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
};

export default productsReducer;
