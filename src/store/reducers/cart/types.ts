import { CartItem, Product } from "../../../utils/models";
import { AnyAction } from "redux";

// Action types

export interface GetLocalCartAction extends AnyAction {
  type: "get-local-cart";
}

export interface AddToCartAction extends AnyAction {
  type: "add-to-cart";
  payload: { product: Product };
}

export interface RemoveFromCartAction extends AnyAction {
  type: "remove-from-cart";
  payload: { productId: Product["id"] };
}

export interface ChangeQuantityAction extends AnyAction {
  type: "change-quantity";
  payload: { productId: Product["id"]; newQuantity: CartItem["quantity"] };
}

export type CartActions =
  | GetLocalCartAction
  | AddToCartAction
  | RemoveFromCartAction
  | ChangeQuantityAction;

// reducer
export type CartState = CartItem[];
