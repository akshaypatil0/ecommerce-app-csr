import cartReducer, {
  addToCart,
  changeQuantity,
  getLocalCart,
  removeFromCart,
} from ".";

const products = getProducts(3);

describe("Cart reducer works", () => {
  it("Should return initial state properly", () => {
    expect(cartReducer(undefined, "dhfsdfk")).toEqual([]);
  });

  it("Should add item to cart", () => {
    const action = addToCart(products[0]);

    expect(cartReducer([], action)).toEqual([
      { product: products[0], quantity: 1 },
    ]);
  });
  it("Should remove item from cart", () => {
    const action = removeFromCart(products[0].id);

    expect(
      cartReducer([{ product: products[0], quantity: 1 }], action)
    ).toEqual([]);
  });
  it("Should change quantity of item", () => {
    const action = changeQuantity(products[0].id, 2);

    expect(
      cartReducer([{ product: products[0], quantity: 1 }], action)
    ).toEqual([{ product: products[0], quantity: 2 }]);
  });
});

describe("Local storage works", () => {
  let state = products.map((product) => ({ product, quantity: 1 }));

  it("Stores state to local storage", () => {
    const newState = cartReducer(
      state,
      addToCart({ id: 6, title: "product 6" })
    );
    expect(localStorage.getItem("cart")).toEqual(JSON.stringify(newState));
  });

  it("Get state from local storage", () => {
    localStorage.setItem("cart", JSON.stringify(state));
    const newState = cartReducer(undefined, getLocalCart());
    expect(newState).toEqual(state);
  });
});
