import { AnyAction } from "redux";
import { Category, Product } from "../../../utils/models";

// Action types

export interface FetchAllCategoriesAction extends AnyAction {
  type: "fetch-all-categories";
  payload: Category[];
  meta: {
    limit?: number;
    force?: boolean;
  };
}

export interface FetchCategoryRequestAction extends AnyAction {
  type: "fetch-category-request";
  meta: {
    category: Category;
  };
}

export interface FetchCategoryAction extends AnyAction {
  type: "fetch-category";
  payload: Product[];
  meta: {
    category: Category;
  };
}

export interface FetchCategoryFailedAction extends AnyAction {
  type: "fetch-category-failed";
  meta: {
    category: Category;
  };
}

export type CategoriesActions =
  | FetchAllCategoriesAction
  | FetchCategoryRequestAction
  | FetchCategoryAction
  | FetchCategoryFailedAction;

// State type
export type CategoriesState = {
  list: Category[];
  lastFetched?: number;
  productsByCategory: {
    [category: string]: {
      list: Product[];
      isFetching: boolean;
      lastFetched?: number;
    };
  };
};
