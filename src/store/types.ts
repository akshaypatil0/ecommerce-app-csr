import { AnyAction } from "redux";
import { ThunkAction } from "redux-thunk";
import {
  ApiActions,
  ApiCallRequestAction,
  ApiState,
} from "./reducers/api/types";
import { CartActions, CartState } from "./reducers/cart/types";
import {
  CategoriesActions,
  CategoriesState,
} from "./reducers/categories/types";
import { ErrorActions, ErrorState } from "./reducers/error/types";
import { ProductActions, ProductState } from "./reducers/product/types";
import { ProductsActions, ProductsState } from "./reducers/products/types";

export interface RootState {
  api: ApiState;
  cart: CartState;
  product: ProductState;
  products: ProductsState;
  categories: CategoriesState;
  error: ErrorState;
}

export type Actions =
  | ApiActions
  | CartActions
  | ProductActions
  | ProductsActions
  | CategoriesActions
  | ErrorActions;
// | ThunkAction<Promise<void>, RootState, {}, ApiCallRequestAction>;
