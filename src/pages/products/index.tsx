import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../store/types";
import { fetchProducts } from "../../store/reducers/products";
import { ProductsState } from "../../store/reducers/products/types";
import { ProductGrid } from "../../components";
import { NextPage } from "next";
import { useEffect } from "react";
import dynamic from "next/dynamic";

const Products: NextPage = () => {
  const { list: products } = useSelector<RootState, ProductsState>(
    (state) => state.products
  );

  const dispatch = useDispatch();
  useEffect(() => {
    if (products && products.length > 5) return;
    dispatch(fetchProducts({ force: true }));
  }, [products, dispatch]);

  return (
    <div>
      <ProductGrid products={products} />
    </div>
  );
};

export default dynamic(() => Promise.resolve(Products), {
  ssr: false,
});

// export default Products;
