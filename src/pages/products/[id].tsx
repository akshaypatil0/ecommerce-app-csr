import { NextPage } from "next";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../store/types";
import { fetchProduct } from "../../store/reducers/product";
import { ProductState } from "../../store/reducers/product/types";

import { AddToCartButton, LoadingSpinner } from "../../components";
import Image from "next/image";

import classes from "../../styles/product.module.scss";
import Head from "../../components/head/head";
import { useEffect } from "react";
import { useRouter } from "next/router";
import dynamic from "next/dynamic";

const Product: NextPage = () => {
  const { data: product, isFetching: isLoading } = useSelector<
    RootState,
    ProductState
  >((state) => state.product);

  const { query } = useRouter();
  const dispatch = useDispatch();

  useEffect(() => {
    if (!query.id) return;
    if (product && product.id === +query.id) return;
    dispatch(fetchProduct(query.id));
  }, [query.id, product, dispatch]);

  if (isLoading || !product) {
    return (
      <div className={classes.product}>
        <Head page="loading..." />
        <LoadingSpinner />;
      </div>
    );
  }

  return (
    <div className={classes.product}>
      <Head page={product.title} description={product.description} />
      <div className={classes.img}>
        <Image
          src={product.image}
          alt={product.title}
          layout="fill"
          objectFit="contain"
        />
      </div>
      <div className={classes.info}>
        <h5>{product.title}</h5>
        <p>{product.description}</p>
        <h6>$ {product.price}</h6>
        <div className={classes.delivery}>
          <h6>Delivery Information</h6>
          <li>Get it within 2 days</li>
          <li>Pay on delivery available</li>
          <li>Easy 30 days return & exchange available</li>
        </div>
        <AddToCartButton product={product} />
      </div>
    </div>
  );
};

export default dynamic(() => Promise.resolve(Product), {
  ssr: false,
});
// export default Product;
