import { DisplayError, Footer, Header } from "../components";
import "../styles/globals.scss";
import { wrapper } from "../store";
import Head from "next/head";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { getLocalCart } from "../store/reducers/cart";

function App({ Component, pageProps }) {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getLocalCart());
  });
  return (
    <>
      <Head>
        <title>Ecommerce app</title>
      </Head>
      <div className="App">
        <Header />
        <DisplayError />
        <div className="container">
          <main>
            <Component {...pageProps} />
          </main>
        </div>
        <Footer />
      </div>
    </>
  );
}

export default wrapper.withRedux(App);
