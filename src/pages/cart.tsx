import { NextPage } from "next";
import dynamic from "next/dynamic";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../store/types";
import { CartState } from "../store/reducers/cart/types";
import { setError } from "../store/reducers/error";
import classes from "../styles/cart.module.scss";
import { CartListItem } from "../components";
import Head from "../components/head/head";
import L10n from "../components/l10n/l10n";

const Cart: NextPage = () => {
  const cart = useSelector<RootState, CartState>((state) => state.cart);
  const [totalPrice, setTotalPrice] = useState(0);

  useEffect(() => {
    let total = cart.reduce(
      (total, { product, quantity }) => total + product.price * quantity,
      0
    );
    setTotalPrice(Math.round(total * 100) / 100);
  }, [cart]);

  const dispatch = useDispatch();
  function handlePlace() {
    dispatch(setError("Cannot place order at this moment, please try again !"));
  }

  if (cart.length < 1) {
    return (
      <div className={classes.emptyCart}>
        <Head page="Cart" />
        <p>Nothing to display in cart !</p>
      </div>
    );
  }
  return (
    <div className={classes.cart}>
      <Head page="Cart" />
      <div className={classes.productList}>
        {cart.map(({ product, quantity }) => (
          <CartListItem
            key={product.id}
            product={product}
            quantity={quantity}
          />
        ))}
      </div>
      <ul className={classes.total}>
        <div className={classes.li}>
          <span>
            <L10n literal="total_price" />:
          </span>
          <span className={classes.num}>
            <L10n price={totalPrice} />
          </span>
        </div>
        <div className={classes.li}>
          <span>
            <L10n literal="delivery_charges" />:
          </span>
          <span className={classes.num}>0</span>
        </div>
        <hr />
        <div className={classes.li}>
          <span>
            <L10n literal="grand_total" />:
          </span>
          <span className={classes.num}>
            <L10n price={totalPrice} />
          </span>
        </div>
        <button onClick={handlePlace}>
          <L10n literal="place_order" />
        </button>
      </ul>
    </div>
  );
};

export default dynamic(() => Promise.resolve(Cart), {
  ssr: false,
});
