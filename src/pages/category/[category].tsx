import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../store/types";
import { fetchProductsByCategory } from "../../store/reducers/categories";
import { CategoriesState } from "../../store/reducers/categories/types";
import { NextPage } from "next";
import { ProductGrid } from "../../components";
import { useRouter } from "next/router";
import { useEffect } from "react";
import dynamic from "next/dynamic";

const CategoryPage: NextPage = () => {
  const { productsByCategory } = useSelector<RootState, CategoriesState>(
    (state) => state.categories
  );
  const { query } = useRouter();
  const category = query.category as string;
  const dispatch = useDispatch();

  useEffect(() => {
    if (!category) return;

    dispatch(fetchProductsByCategory({ category, force: true }));
  }, [category, dispatch]);

  return (
    <div>
      <ProductGrid products={productsByCategory[category].list} />
    </div>
  );
};

export default dynamic(() => Promise.resolve(CategoryPage), {
  ssr: false,
});
// export default CategoryPage;
