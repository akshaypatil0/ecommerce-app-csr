import React, { FC } from "react";
import L10n from "../l10n/l10n";
import classes from "./add-to-cart.module.scss";

const AddToCartButton: FC<Props> = ({ handleAdd, isAdded }) => {
  return (
    <button
      className={classes.addToCart}
      onClick={handleAdd}
      disabled={isAdded}
    >
      {isAdded ? (
        <L10n literal="added_to_cart" />
      ) : (
        <L10n literal="add_to_cart" />
      )}
    </button>
  );
};

interface Props {
  handleAdd: () => void;
  isAdded: boolean;
}

export default AddToCartButton;
