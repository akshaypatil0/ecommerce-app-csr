import React, { FC } from "react";
import Link from "next/link";
import Image from "next/image";
import { Product } from "../../utils/models";
import { AddToCartButton } from "..";
import classes from "./product-tile.module.scss";
import L10n from "../l10n/l10n";

const ProductTile: FC<Props> = ({ product }) => {
  return (
    <Link href={`/products/${product.id}`} passHref>
      <div className={classes.productTile}>
        <Image
          src={product.image}
          alt={product.title}
          height={150}
          width={150}
          // layout="responsive"
        />
        <div className={classes.info}>
          <h2 className={classes.title}>{product.title}</h2>
          <h3 className={classes.price}>
            <L10n price={product.price} />
          </h3>
        </div>
        <AddToCartButton product={product} />
      </div>
    </Link>
  );
};

interface Props {
  product: Product;
}

export default ProductTile;
