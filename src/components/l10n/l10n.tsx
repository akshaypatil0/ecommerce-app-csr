import { useRouter } from "next/router";
import { useEffect } from "react";
import { useState } from "react";
import { useCallback } from "react";
import { FC } from "react";
import en_in_src from "../../i18n/en_in_src";
import en_src from "../../i18n/en_src";
import hi_src from "../../i18n/hi_src";
import { Literals, LiteralSrc } from "../../i18n/literals.types";

const L10n: FC<Props> = ({ literal, cue, price }) => {
  const [str, setStr] = useState("");
  const { locale } = useRouter();

  const getSrc = useCallback((): LiteralSrc => {
    switch (locale) {
      case "hi":
        return hi_src;

      case "en":
        return en_src;

      default:
        return en_in_src;
    }
  }, [locale]);

  useEffect(() => {
    const src = getSrc();

    if (price !== undefined) {
      setStr(src.convertCurrency(price));
    } else {
      setStr(src[literal](cue));
    }
  }, [literal, cue, price, getSrc]);

  return <>{str}</>;
};

interface LiteralProps {
  literal: Literals;
  cue?: string;
  price?: never;
}

interface PriceProps {
  price: number;
  literal?: never;
  cue?: never;
}

type Props = LiteralProps | PriceProps;

export default L10n;
