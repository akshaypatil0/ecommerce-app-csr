export type Literals =
  | "featured_products"
  | "explore"
  | "see_more"
  | "home"
  | "cart"
  | "contact"
  | "add_to_cart"
  | "added_to_cart"
  | "remove_from_cart"
  | "place_order"
  | "total"
  | "total_price"
  | "delivery_charges"
  | "grand_total"
  | "name"
  | "email"
  | "message"
  | "send";

export type LiteralSrc = {
  [literal in Literals]: (cue?: string) => string;
} & {
  convertCurrency: (c: number) => string;
};
