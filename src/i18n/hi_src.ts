import { LiteralSrc } from "./literals.types";

const hi_src: LiteralSrc = {
  featured_products: () => "विशेष वस्तुएं",
  explore: (key) => `${key} में खोजिये`,
  see_more: (key) => (key ? `${key} में और देखें` : "और वस्तुएं देखें"),
  home: () => "होम",
  cart: () => "कार्ट",
  contact: () => "संपर्क",
  add_to_cart: () => "कार्ट में जोड़ें",
  added_to_cart: () => "कार्ट में जोड़ा गया",
  remove_from_cart: () => "कार्ट से निकालें",
  total: () => "जोड़",
  total_price: () => "कुल कीमत",
  delivery_charges: () => "वितरण शुल्क",
  grand_total: () => "कुल जोड़",
  place_order: () => "आर्डर करे",
  name: () => "नाम",
  email: () => "ईमेल",
  message: () => "सन्देश",
  send: () => "भेजें",

  convertCurrency: (c) => `₹ ${Math.floor(c * 74.27 * 100) / 100}`,
};

export default hi_src;
