import { LiteralSrc } from "./literals.types";

const en_src: LiteralSrc = {
  featured_products: () => "Featured Products",
  explore: (key) => `Explore ${key}`,
  see_more: (key) => (key ? `See more in ${key}` : "See more products"),
  home: () => "Home",
  cart: () => "Cart",
  contact: () => "Contact",
  add_to_cart: () => "Add to cart",
  added_to_cart: () => "Added to cart",
  remove_from_cart: () => "Remove from cart",
  total: () => "Total",
  total_price: () => "Total price",
  delivery_charges: () => "Delivery charges",
  grand_total: () => "Grand total",
  place_order: () => "Place order",
  name: () => "Name",
  email: () => "Email",
  message: () => "Message",
  send: () => "Send",

  convertCurrency: (c) => `$ ${c}`,
};

export default en_src;
